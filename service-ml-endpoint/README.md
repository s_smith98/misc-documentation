# SERVICE-ML-ENDPOINT DOCUMENTATION

Date: April 27, 2021

Author: Steven Smith

For: Plusgrade service-ml-endpoint lambda function

Last Update: N/A

# Table of Content
- [SERVICE-ML-ENDPOINT DOCUMENTATION](#service-ml-endpoint-documentation)
- [Table of Content](#table-of-content)
- [1. Description](#1-description)
- [2. Features](#2-features)
  - [2.1 Json Schema](#21-json-schema)
  - [2.2 Batch Prediction](#22-batch-prediction)
- [3. Permsissions](#3-permsissions)
- [4. Layers](#40-layers)

# 1. Description

Service-ml-endpoint interacts with the sagemaker endpoint created for labs-pipeline, or any other specified in the system environment. Its purpose is to validate api requests and format the data that is sent to the endpoint via `invoke_endpoint`. 

# 2. Features

* JSON Schema validation
* Batch predictions, up to sixty-two requests at once.
  

## 2.1 Json Schema

The lambda uses `from jsonschema import validate, draft7_format_checker` to validate the body of the lambda event. 
This ensures that the body of the request conforms to requirements of the function. Below is the current Json schema used:

    {
        "$id": "http://json-schema.org/draft-07/schema#",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "title":"Endpoint Request",
        "description":"A request to the machine learning endpoint",
        "type":"array",
        "items":{
            "type":"object",
            "properties":{
                "origin": {
                    "description":"the flight origin",
                    "type":"string"
                },
                "dest": {
                    "description":"the flight destination",
                    "type":"string"
                },
                "carrier": {
                    "description":"the flight carrier",
                    "type":"string"
                },
                "currency": {
                    "description":"the currency used at the point of sale",
                    "type":"string"
                },
                "fare_class": {
                    "departure": "The class of the seat",
                    "type": "string"
                },
                "point_of_sale": {
                    "description":"the point of sale of the flight",
                    "type":"string"
                },
                "upgrade_type": {
                    "description":"the type of seat the user is being upgrade to",
                    "type":"string"
                },
                "departure_time_hour": {
                    "description":"the hour of departure",
                    "type":"integer"
                },
                "departure_time_day_of_week": {
                    "description":"the day of week of departure",
                    "type":"integer"
                },
                "departure_time_day_of_month": {
                    "description":"the day of the month of departure",
                    "type":"integer"
                },
                "departure_time_day_of_year": {
                    "description":"the day in the year of departure",
                    "type":"integer"
                },
                "departure_time_month": {
                    "description":"the monthr of departure",
                    "type":"integer"
                },
                "slider_min_usd": {
                    "description":"",
                    "type":"integer"
                },
                "slider_max_usd": {
                    "description":"the number of passenger in the upgrade",
                    "type":"integer"
                }
            },
            "required":["origin", "dest", "carrier","currency","point_of_sale","upgrade_type","departure_time_hour","departure_time_day_of_week","departure_time_day_of_month","departure_time_day_of_year","departure_time_month","slider_max_usd","slider_min_usd"],
            "additionalProperties": false
        },
        "minItems":1,
        "maxItems": 62,
        "uniqueItems": true
    }

The schema expects the body to be a JSON array of objects with the required properpeties: 

* origin
* dest
* carrier
* currency
* fare_class
* point_of_sale
* upgrade_type
* departure_time_hour
* departure_time_day_of_week
* departure_time_day_of_month
* departure_time_day_of_year
* depature_time_month
* slider_min_usd
* slider_max_usd
  
Furthermore, the schema validation will return an error if any object in the array is a duplicate or if they contain illegal (unspecified) properties. 

## 2.2 Batch Prediction

The batch prediction support is a kind of workaround of the limitations of `invoke_endpoint` which only accepts a payload in the form of a text/csv with a single row. 

As the JSON Schema specifies the JSON array requires at least one object up to a maximum of sixty-two. The lambda will loop over the objects in the request, format it into a text/csv input for the predictor and call `invoke_endpoint`. The result are stored in a list that is returned to the client. 

# 3. Permsissions

A list of permissions that must be granted to the role:

- `{
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "sagemaker:InvokeEndpoint",
            "Resource": "*"
    }`

# 4. Layers
- numpy-pandas-layer
- JsonSchema