# LABS-AUTOMATE-ML-SERVICE DOCUMENTATION
Date: April 29, 2021

Author: Steven Smith

For: Plusgrade labs-automate-ml-service lambda

Last Update: N/A

# Table of Content
- [LABS-AUTOMATE-ML-SERVICE DOCUMENTATION](#labs-automate-ml-service-documentation)
- [Table of Content](#table-of-content)
- [1. Description](#1-description)
- [2. Schedule](#2-schedule)
- [3. Permissions](#3-permissions)

# 1. Description

labs-automate-ml-service is a scheduled lambda function that executes at 0:00 UTC every day, The job of the lambda function is to execute the labs-pipeline machine learning pipeline.

# 2. Schedule

The schedule is configured using cloud watch events, now known as event bridge. It uses a chron expressions to execute the lambda every day at exactly 00:00 UTC. 

# 3. Permissions

The lambda function requires permissions to execute the pipeline. Therefore, it will require permission for this action `StartPipelineExecution`. 